# e360 0.2.2

- add table ausbauetappen erz into e360 dm


# e360 0.2.1

- import {gert} and use in `deploy_package()`


# e360 0.2.0

- rewrite `deploy_package()` to add compatibility for RSC deployment


# e360 0.1.1.9013

- Allow ../drat.


# e360 0.1.1.9012

- add table stilllegungsperimeter and -zonen


# e360 0.1.1.9011

- deploy helpers: small optimizations


# e360 0.1.1.9010

- Add missing link.


# e360 0.1.1.9009

- Link geraet and components.


# e360 0.1.1.9008

- Add `components` table to data model.
- Fix url in `deploy_pkgdown()`.


# e360 0.1.1.9007

- New `e360_sap_dm()`.


# e360 0.1.1.9006

- add `deploy_pkgdown()`


# e360 0.1.1.9005

- Use `st_as_text()` to work around https://github.com/r-dbi/odbc/issues/373.


# e360 0.1.1.9004

- Set IMPLICIT_TRANSACTIONS to OFF for connections.


# e360 0.1.1.9003

- Work around odd memoise problem with an explicit caching environment.


# e360 0.1.1.9002

- Memoise `e360_dm()`.
- `e360_dm()` keeps binary version of `SHAPE` column as `SHAPE_MSSQL`.
- `as_shape()` gains `old_name = NULL` argument.


# e360 0.1.1.9001

- Avoid using deprecated functions internally.
- Export `e360_db()` and `e360_tbl()`.


# e360 0.1.1.9000

- Same as previous version.


# e360 0.1.1

- Same as previous version.


# e360 0.1.0.9020

- New `e360_db()` and `e360_tbl()`, deprecate `e360_db_*()` and `e360_tbl_*()`.


# e360 0.1.0.9019

- Buchwert Leitungen über virtuelle Zwischentabelle hinzugefügt.


# e360 0.1.0.9018

- Same as previous version.


# e360 0.1.0.9017

- Add link table between `leitungen` and `perimeter_...`.


# e360 0.1.0.9016

- Adapt to dm 0.1.1.


# e360 0.1.0.9015

- Fix `ui_path()`.
- Import {dm}.


# e360 0.1.0.9014

- `sf_copy_to()` calls `compute()` for permanent tables.


# e360 0.1.0.9013

- New `e360_dm()`.


# e360 0.1.0.9012

- `find_nearest()` gains `max_dist` and `top` arguments.


# e360 0.1.0.9011

- Fix for `ui_done()` etc.


# e360 0.1.0.9010

- Make usethis optional.


# e360 0.1.0.9009

- Qualified usethis access.


# e360 0.1.0.9008

- Don't import usethis.


# e360 0.1.0.9007

- Fix class for copy_to() method.


# e360 0.1.0.9006

- New `pick_nearest()`.


# e360 0.1.0.9005

- `find_nearest()` gains `select` argument to select columns from the haystack table.
- `find_nearest()` uses first column as row identifier.


# e360 0.1.0.9004

- New `find_nearest()`.
- New `sf_copy_to()`, currently can't `collect()` data returned from it.
- `default_shape()` now recognizes a `SHAPE` column.


# e360 0.1.0.9003

- New `deploy_package()`.


# e360 0.1.0.9002

- Same as previous version.


# e360 0.1.0.9001

- Zip while still a list, geometries are loaded much faster.


# e360 0.1.0.9000

- New `shape_to_end()`.
- Export `to_wgs84()` and `to_wgs84_aux()`.
- New `collect_shape_as_sf()`.
- Implement `copy_to()`.
- Rename `geocode()` to `e360_geocode()`.
- New `e360_db_*()` (connects only once per session) and `e360_tbl_*()`.


