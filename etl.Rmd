---
title: "Import Gasmarkt-Datenmodell"
output: rmarkdown::html_document
date: "`r format(Sys.time(), '%c')`"
resource_files:
- DESCRIPTION
- NAMESPACE
- R/*
editor_options:
  chunk_output_type: console
---

```{r setup, include=FALSE}
library(DBI)
library(odbc)

library(tidyverse)
library(dbplyr)
library(viridis)
library(purrr)
library(glue)
library(dm)
requireNamespace("DiagrammeR")

knitr::opts_chunk$set(
  error = TRUE,
  fig.height = 0.3,
  fig.width = 10
)

knit_print.grViz <- function(x, ...) {
  x %>%
    DiagrammeRsvg::export_svg() %>%
    c("`````{=html}\n", ., "\n`````\n") %>%
    knitr::asis_output()
}

vctrs::s3_register("knitr::knit_print", "grViz")
```

```{r load-all, include = FALSE}
# For rsconnect
if (FALSE) {
  library(mapsapi)
  library(roxygen2)
}
pkgload::load_all()
```


```{r tables, include=FALSE}
gass_sap_hap_pkt <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_hap_pkt") %>%
  select(-OID_recz) %>%
  collect_as_sf()

gass_sap_anschlussobjekte_pkt <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_anschlussobjekte_pkt") %>%
  collect_as_sf()

gass_sap_anlage <- e360_tbl("gmw_datawarehouse.e360.u_gass_sap_anlage") %>%
  select(-OID_recz) %>%
  rename(anlage_id = anlage) %>%
  collect()

gass_sap_verbrauch <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_verbrauch") %>%
  select(-OID_recz) %>%
  rename(
    anlage_id = anlage
  ) %>%
  collect()

gass_sap_verbrauch_10j <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_verbrauch_10j") %>%
  filter(between(gjahr, 2015, 2017)) %>%
  select(-OID_recz) %>%
  rename(
    anlage_id = anlage
  ) %>%
  collect()

gass_sap_geraet <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_geraet") %>%
  select(-OID_recz) %>%
  rename(
    geraet_id = equnr,
    anlage_id = anlage,
    objektart_id = objektart
  ) %>%
  collect()

gass_sap_geschaeftspartner <-
  e360_tbl("gmw_datawarehouse.e360.u_gass_sap_geschaeftspartner") %>%
  select(-OID_recz) %>%
  rename(
    anlage_id = anlage,
    vertrag_id = vertrag,
    rolle_id = rolle
  ) %>%
  collect()

gass_sap_objektarten <-
  e360_tbl("gmw_datawarehouse.e360.sap_objektarten") %>%
  rename(objektart_id = objektart) %>%
  collect()

overview_import <- tibble(
  Table = c("hap_pkt", "anschlussobjekte_pkt", "anlage", "verbrauch", "verbrauch_10j", "geraet", "geschaeftspartner", "objekt"),
  Rows = c(
    nrow(gass_sap_hap_pkt),
    nrow(gass_sap_anschlussobjekte_pkt),
    nrow(gass_sap_anlage),
    nrow(gass_sap_verbrauch),
    nrow(gass_sap_verbrauch_10j),
    nrow(gass_sap_geraet),
    nrow(gass_sap_geschaeftspartner),
    nrow(gass_sap_objektarten)
  )
)
```

Import beendet, importierte Zeilen pro Tabelle:

```{r import_rows}
overview_import
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE, eval=FALSE}
gass_sap_verbrauch_10j %>%
  anti_join(gass_sap_anlage, by = "anlage_id")

gass_sap_verbrauch_10j %>%
  anti_join(gass_sap_anlage, by = "anlage_id") %>%
  anti_join(gass_sap_hap_pkt, by = "aobj")
```


```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
gass_sap_verbrauch_all <- 
  bind_rows(
    # Anlage-IDs noch inkonsistent, wir bräuchten auch hier die 10-Jahres-Historie
    gass_sap_verbrauch_10j %>%
      semi_join(gass_sap_anlage, by = "anlage_id"),
    gass_sap_verbrauch
  )
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
gass_sap_anlage_nonempty <-
  gass_sap_anlage %>%
  filter(!is.na(anlage_id))
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
anlage_oid_to_remove <-
  gass_sap_anlage_nonempty %>%
  group_by(anlage_id, aobj) %>%
  mutate(n = n()) %>%
  ungroup() %>%
  filter(n > 1) %>%
  filter(str_detect(zaehlpunkt, "^[^ ]+ |^  ")) %>%
  pull(OBJECTID)

# anlage: mehr als eine Anlage am HAP,
# zum Teil die gleiche Anlage an mehreren HAP (Unterzähler)

if (length(anlage_oid_to_remove) == 0) {
  anlage_full <- gass_sap_anlage_nonempty
} else {
  anlage_full <-
    gass_sap_anlage_nonempty %>%
    filter(!(OBJECTID %in% !!anlage_oid_to_remove))
}
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
anlage_verbrauch <- 
  gass_sap_verbrauch_all %>%
  select(anlage_id, aobj) %>% 
  distinct() %>% 
  mutate(is_verbrauch = TRUE)

anlage_pkt_link <-
  anlage_full %>%
  select(anlage_id, aobj) %>%
  mutate(is_anlage = TRUE) %>% 
  full_join(anlage_verbrauch, by = c("aobj", "anlage_id")) %>% 
  mutate(across(c(is_anlage, is_verbrauch), coalesce, FALSE))

# FIXME: is_anlage & !is_verbrauch anschauen -- Fehler?
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
anlage <-
  anlage_full %>%
  select(-aobj) %>%
  select(-OBJECTID, OBJECTID) %>% # this moves OBJECTID to the end
  group_by(
    anlage_id, anlart, tariftyp, branche, zaehlpunkt, preisstaffel, bio_proz,
    zdem_inst1, zdem_indv1, zdem_indv1_bis, abrechnungsklasse, MD_prozessiert
  ) %>%
  arrange(anlage_id) %>%
  mutate(n = row_number()) %>%
  ungroup() %>%
  filter(n == 1) %>%
  select(-n)
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
verbrauch <-
  gass_sap_verbrauch_all %>% 
  select(-aobj) %>%
  arrange(anlage_id, gjahr) %>%
  distinct(anlage_id, gjahr, .keep_all = TRUE) %>% 
  mutate(verbrauch_id = row_number())
```

```{r, warning=FALSE}
hap_pkt_all <-
  gass_sap_hap_pkt %>%
  filter(MD_gueltig_aktuell == 1) %>%
  select(aobj, str, haus_num1, haus_zusatz, plz, ort, str_code, ort_code, SHAPE)

hap_pkt_mit_adresse <-
  hap_pkt_all %>%
  as_tibble() %>%
  mutate(SHAPE = sf::st_sfc(SHAPE, crs = 2056)) %>%
  sf::st_as_sf() %>%
  # only those that occur in anlage
  mutate(pkt_status = "hap")
# "warning in bind_rows: Vectorizing 'sfc_POINT' elements may not preserve their attributes"

# Manche `aobj` sind nicht in HAP enthalten, aber in anschlussobjekte
# FIXME: there are currently (21. Nov 2019) no cases which are not in HAP + hap_pkt_empty_tbl but in anschlussobjekte
# only has an effect if not including hap_pkt_empty_tbl, then 811 aobj come from anschlussobjekte, 27022 from hap
ao_pkt_mit_adresse <-
  # only those `aobj` from `anlage` that are missing in `HAP`
  anti_join(gass_sap_anschlussobjekte_pkt, un_sf(hap_pkt_mit_adresse), by = "aobj") %>%
  semi_join(anlage_pkt_link, by = "aobj") %>%
  select(aobj, SHAPE, str, haus_num1, haus_zusatz, plz, ort, gwr_egid) %>%
  mutate(pkt_status = "ao")

# combine:
pkt_mit_adresse <-
  as_tibble(hap_pkt_mit_adresse) %>%
  bind_rows(as_tibble(ao_pkt_mit_adresse)) %>%
  sf::st_as_sf()

# create lookup table for addresses:
c_p_list <- decompose_table(
  .data = as_tibble(pkt_mit_adresse),
  new_id_column = pkt_adresse_id,
  str, haus_num1, haus_zusatz, plz, ort, str_code, ort_code
)
pkt <-
  c_p_list[["child_table"]] %>%
  select(-SHAPE, everything()) %>%
  sf::st_as_sf(crs = 2056)
pkt_adresse <- c_p_list[["parent_table"]]
```

```{r , warning=FALSE, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
anlage_pkt <-
  pkt %>%
  filter(!sf::st_is_empty(SHAPE)) %>% # FIXME: empty SHAPE entries need to be filtered out
  left_join(anlage_pkt_link %>% add_count(anlage_id), by = "aobj") %>%
  filter(n > 1) %>%
  arrange(-n) %>%
  group_by(anlage_id) %>%
  summarise(do_union = FALSE) %>% # Typstabilität: ergibt multipoint auch für nur einen Punkt
  mutate(do_union = NULL)
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
# Alle Geschäftspartner unabhängig von auszdat
geschaeftspartner <-
  gass_sap_geschaeftspartner

# 1. aobj entfernen
# zudem wird nur die erste Reihe der bei `distinct()` vorgegebenen Kombination aus Attributen behalten, falls sie öfter vorkommt.
gp_anlage <-
  geschaeftspartner %>%
  select(-aobj) %>%
  rename(gpartner_id = gpartner) %>%
  distinct(anlage_id, rolle_id, vertrag_id, gpartner_id, .keep_all = TRUE) %>%
  filter(!is.na(gpartner_id))
# bei Testdatensatz reduziert sich die Zeilenzahl durch `distinct()` von 140848 auf 140799

gpartner_mit_adresse <-
  gp_anlage %>%
  select(-OBJECTID, -anlage_id, -vertrag_id, -einzdat, -auszdat, -rolle_id) %>%
  distinct()

# Lookup-Tabelle für die Adressen der Geschäftspartner
gpartner_adresse <-
  gpartner_mit_adresse %>%
  distinct(
    strasse,
    hausnr,
    plz,
    ort
  ) %>%
  mutate(gpartner_adresse_id = row_number()) %>%
  select(gpartner_adresse_id, everything())

# Detail-Tabelle für Geschäftspartner
gpartner <-
  gpartner_mit_adresse %>%
  left_join(gpartner_adresse,
    by = c(
      "strasse",
      "hausnr",
      "plz",
      "ort"
    )
  ) %>%
  select(
    -strasse,
    -hausnr,
    -plz,
    -ort
  ) %>%
  select(
    gpartner_id,
    gpartner_adresse_id,
    everything()
  )

# 2. Verknüpfungstabelle zwischen Geschäftspartnern
gp_link <-
  gp_anlage %>%
  select(anlage_id, vertrag_id, einzdat, auszdat, rolle_id, gpartner_id) %>%
  mutate(einzdat = as.Date(einzdat)) %>%
  mutate(auszdat = as.Date(auszdat))
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
# Vertrag: unabhängig von Geschäftspartner und Rolle des Geschäftspartners
vertrag <-
  gp_link %>%
  distinct(vertrag_id, anlage_id, einzdat, auszdat) %>%
  filter(!is.na(vertrag_id))

# Link zwischen Vertragsnummer und Geschäftspartner und Rolle des Geschäftspartners
gpartner_rolle_vertrag_link <-
  gp_link %>%
  select(vertrag_id, gpartner_id, rolle_id)
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
objektart <- gass_sap_objektarten
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
rolle <- tribble(
  ~rolle_id, ~rolle_lookup,
  "AwRE", "Abweichender Rechnungsempfänger",
  "EG", "Eigentümer",
  "GP", "Geschäftspartner",
  "VG", "Verwaltung"
)
```

```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
geraet_orig <-
  gass_sap_geraet %>%
  filter(ausbau == as.Date("9999-12-31"))
```


```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE, eval=FALSE}
geraet_orig %>%
  add_count(geraet_id) %>%
  filter(n > 1) %>%
  arrange(geraet_id)
```


```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
# equnr: Primärschlüssel
geraet_good_bad <-
  geraet_orig %>%
  select(-globalid, -anlage_id) %>%
  distinct(geraet_id, .keep_all = TRUE) %>%
  # FIXME: Kompatibilität mit finot
  rename(aobj0 = aobj, tplnr0 = tplnr) %>%
  mutate(tplnr = gsub(" .*$", "", tplnr0)) %>%
  mutate(aobj = if_else(tplnr0 != "" & tplnr0 %in% pkt$aobj, tplnr0, aobj0)) %>%
  select(geraet_id, everything()) %>%
  select(-OBJECTID, everything(), OBJECTID)

# Linktabelle für Geräte
anlage_geraet_link <-
  geraet_orig %>%
  select(geraet_id, anlage_id) %>%
  filter(!is.na(anlage_id))
```


```{r}
# Geräte ohne Objektart
geraet_objektart_id_bad <-
  geraet_good_bad %>%
  filter(objektart_id == "")

geraet_objektart_id_bad
```

```{r}
# Objektart mit NULL überschreiben
geraet <-
  geraet_good_bad %>%
  mutate(objektart_id = if_else(objektart_id == "", NA_character_, objektart_id))
```



```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
anlage_pkt_edges <-
  anlage_pkt_link %>%
  transmute(
    from = paste0("anl-", anlage_id),
    to = paste0("pkt-", aobj)
  )

anlage_geraet_edges <-
  anlage_geraet_link %>%
  transmute(
    from = paste0("anl-", anlage_id),
    to = paste0("ger-", geraet_id)
  )

pkt_geraet_edges <-
  geraet %>%
  transmute(
    from = paste0("pkt-", aobj),
    to = paste0("ger-", geraet_id)
  )

graph <-
  bind_rows(anlage_pkt_edges, anlage_geraet_edges, pkt_geraet_edges) %>%
  igraph::graph_from_data_frame(directed = FALSE)

graph_components <- igraph::components(graph)

graph_components_df <-
  graph_components$membership %>%
  enframe("id", "component_id")
```


```{r, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
components <-
  graph_components_df %>%
  distinct(component_id)

anlage_components_link <-
  graph_components_df %>%
  filter(str_detect(id, "^anl-")) %>%
  transmute(anlage_id = as.integer(gsub("^anl-", "", id)), component_id)

pkt_components_link <-
  graph_components_df %>%
  filter(str_detect(id, "^pkt-")) %>%
  transmute(aobj = gsub("^pkt-", "", id), component_id)

geraet_components_link <-
  graph_components_df %>%
  filter(str_detect(id, "^ger-")) %>%
  transmute(geraet_id = as.integer(gsub("^ger-", "", id)), component_id)

anlage <-
  anlage %>%
  left_join(anlage_components_link, by = "anlage_id")

pkt <-
  pkt %>%
  left_join(pkt_components_link, by = "aobj")

geraet <-
  geraet %>%
  left_join(geraet_components_link, by = "geraet_id")
```


```{r, echo=FALSE, fig.height=7, echo=FALSE, results="hide", message=FALSE, warning=FALSE}
gasmarkt_raw <- dm(
  gpartner_rolle_vertrag_link,
  anlage_geraet_link,
  anlage_pkt_link,
  verbrauch,
  vertrag,
  gpartner,
  rolle,
  geraet,
  anlage,
  pkt,
  gpartner_adresse,
  pkt_adresse,
  objektart,
  components,
) %>%
  # gasmarkt_add_colors_pkt() %>%
  dm_add_pk(pkt, aobj) %>%
  dm_add_pk(pkt_adresse, pkt_adresse_id) %>%
  dm_add_pk(anlage, anlage_id) %>%
  dm_add_pk(verbrauch, verbrauch_id) %>%
  dm_add_pk(geraet, geraet_id) %>%
  dm_add_pk(objektart, objektart_id) %>%
  dm_add_pk(rolle, rolle_id) %>%
  dm_add_pk(vertrag, vertrag_id) %>%
  dm_add_pk(gpartner, gpartner_id) %>%
  dm_add_pk(gpartner_adresse, gpartner_adresse_id) %>%
  dm_add_pk(components, component_id) %>%

  dm_add_fk(geraet, objektart_id, objektart) %>%
  # dm_add_fk(geraet, aobj, pkt) %>% # inconsistent
  # Column `objektart_id` in table `geraet` contains values that are not present in column `objektart_id` in table `objektart`
  dm_add_fk(anlage_geraet_link, geraet_id, geraet) %>%
  dm_add_fk(anlage_geraet_link, anlage_id, anlage) %>%
  dm_add_fk(verbrauch, anlage_id, anlage) %>%
  dm_add_fk(vertrag, anlage_id, anlage) %>%
  dm_add_fk(anlage_pkt_link, anlage_id, anlage) %>%
  dm_add_fk(gpartner_rolle_vertrag_link, vertrag_id, vertrag) %>%
  dm_add_fk(gpartner_rolle_vertrag_link, gpartner_id, gpartner) %>%
  dm_add_fk(gpartner_rolle_vertrag_link, rolle_id, rolle) %>%
  dm_add_fk(gpartner, gpartner_adresse_id, gpartner_adresse) %>%
  dm_add_fk(anlage_pkt_link, aobj, pkt) %>%
  dm_add_fk(pkt, pkt_adresse_id, pkt_adresse) %>%

  dm_add_fk(anlage, component_id, components) %>%
  dm_add_fk(pkt, component_id, components) %>%
  dm_add_fk(geraet, component_id, components)
```

- Datenmodell erstellt

```{r draw}
gasmarkt_raw %>%
  dm_draw()

gasmarkt_raw %>%
  dm_get_tables() %>%
  map(map, class)
```

- Löschen fehlender Verbindungen zwischen Anlage und HAP-Pkt (FIXME: Punkte nachführen)

```{r make_consistent}
gasmarkt_raw %>%
  dm_zoom_to(anlage_pkt_link) %>%
  anti_join(pkt)

gasmarkt <-
  gasmarkt_raw %>%
  dm_zoom_to(anlage_pkt_link) %>%
  semi_join(pkt) %>%
  dm_update_zoomed()
```


```{r quality, echo=FALSE, warning=FALSE, message=FALSE}
# Überprüfung, welche Schlüsselbeziehungen erfüllt oder verletzt sind
pkt_un_sf <- un_sf(pkt)
dm_quality_report <-
  gasmarkt %>%
  dm_examine_constraints()
```

Erfüllung der Schlüssel-Constraints:

```{r quality_report}
dm_quality_report
```

```{r quality_report_check}
dm_is_consistent <- FALSE
dm_is_consistent <- all(dm_quality_report$is_key) && !is.null(dm_quality_report$is_key)
stopifnot(dm_is_consistent)
```




- Alte Tabellen auf DB gelöscht

```{r table-names, echo=FALSE, results="hide", message=FALSE, warning=FALSE, eval=dm_is_consistent}
db_name <- "data_analysis"
schema_name <- "gasmarkt"

schema_fun <- function(x) {
  DBI::SQL(set_names(paste0(db_name, ".", schema_name, ".", x), x))
}

# Namensvektor mit den Namen der Tabellen auf der DB, bestehend aus DB-Name "data_analysis", Schemaname "gasmarkt" und Tabellenname.
table_db_names <- schema_fun(names(gasmarkt))
```


```{r delete, echo=FALSE, message=TRUE, eval=dm_is_consistent}
dbExecute(e360_db(permission = "manage"), paste0("USE ", db_name), immediate = TRUE)

dbExecute(e360_db(permission = "manage"), paste0("CREATE SCHEMA ", schema_name), immediate = TRUE)

# Löschen der bisherigen Tabellen, damit die neuen Tabellen geschrieben werden können:
walk(
  table_db_names,
  ~ possibly(dbExecute, NULL, quiet = FALSE)(
    e360_db(permission = "manage"), glue("DROP TABLE {..1}"))
)
```

- Neue Tabellen geschrieben

```{r copy, echo=FALSE, results="hide", message=FALSE, warning=FALSE, eval=dm_is_consistent}
# Kopieren des `dm` Objekts auf die DB:
gasmarkt_db <- copy_dm_to(
  e360_db(permission = "manage"),
  gasmarkt,
  table_names = schema_fun,
  temporary = FALSE,
  
  # FIXME!
  set_key_constraints = FALSE
)
```

Schreiben auf die DB "data_analysis", Schema "gasmarkt" beendet. Geschriebene Zeilen pro Tabelle:

```{r check_written_rows, echo=FALSE, results="hide", message=FALSE, warning=FALSE, eval=dm_is_consistent}
written_DB_rows <-
  map_int(dm_get_tables(gasmarkt_db), ~ as.integer(pull(count(.)))) %>%
  enframe(name = "Table", value = "Rows")
```

```{r written_rows}
written_DB_rows
```

Zugriff auf das Datenmodell via `e360_dm()`:

```{r error = TRUE}
e360_dm() %>% 
  dm_nrow() %>% 
  enframe(name = "Table", value = "Rows")
```

## Session-Info

```{r}
sessioninfo::session_info()
```
