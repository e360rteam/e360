# e360

<!-- badges: start -->
<!-- badges: end -->

## Installation

You can install the released version of e360 from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("e360")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(e360)
## basic example code
```

## Environment variables

- `E360_DB_DWH_UID` and `E360_DB_DWH_PWD`: User ID and password for DWH database, used in `e360_db_dwh()`
- `E360_DB_ANALYSIS_UID` and `E360_DB_ANALYSIS_PWD`: User ID and password for data analysis database, used in `e360_db_analysis()`
- `E360_DB_ADRESS_UID` and `E360_DB_ADRESS_PWD`, used in `e360_db_adress()`
- `E360_KEY_GOOGLEMAPS`: API key for Google Maps
