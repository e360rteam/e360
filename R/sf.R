# https://github.com/r-spatial/sf/pull/917
sf_multipoint_to_point <- function(.data, shape_name = !!default_shape(.data)) {
  shape <- .data[[shape_name]]
  shape[sf::st_is_empty(shape)] <- sf::st_point()
  shape <- sf::st_cast(shape, "POINT")
  .data[[shape_name]] <- shape
  .data
}

#' @importFrom tibble as_tibble
#' @export
as_tibble.sf <- function(x, ...) {
  class(x) <- setdiff(class(x), "sf")
  NextMethod()
}

#' @export
un_sf <- function(.data) {
  .data %>%
    as_tibble() %>%
    select_if(~ !inherits(., "sfc"))
}
