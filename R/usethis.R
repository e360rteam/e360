is_usethis_installed <- function() {
  is_installed("usethis")
}

ui_url <- function(x) {
  ui_path(x, base = NA)
}

ui_path <- function(x, base = NULL) {
  if (!is_usethis_installed()) {
    return(x)
  }
  usethis::ui_path(x, base = base)
}

ui_done <- function(x, .envir = parent.frame()) {
  if (!is_usethis_installed()) {
    return(x)
  }
  usethis::ui_done(x, .envir)
}
