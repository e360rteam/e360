#' @title gibt summierten Gasverbrauch für ein bestimmtes Anschlussobjekt zurück
#'
#' @description kann auch für > 1 Anschlussobjekte ausgeführt werden. Dabei allenfalls
#' duplizierte Verbräuche (eine Anlage kann zu mehreren Anschlussobjekten gehören) werden
#' automatisch entfernt und der entsprechende Verbrauch einem zufälligen Anschlussobjekt zugewiesen.
#'
#' @details zurückgegeben werden die werte erdgas_tr_pkt (erdgas durchgeleitet), erdgas_ha_pkt
#' (erdgas gehandelt) und biogas_pkt. Alle Werte sind in kWh. Zusätzlich gruppiert wird nach
#' Geschäftsjahr und Abrechnungsklasse (100 = Energie 360° AG, 130 = Energie 360 Schweiz AG).
#'
#' @name verbrauch_pkt
#' @param pkt Amschlussobjekt
#' @examples
#' verbrauch_pkt(pkt = c("Z/0001.00001.0138", "Z/0001.01731.0099"))
#' @export
verbrauch_pkt <- function(pkt) {
  dm_verbrauch <- e360_dm() %>%
    dm_select_tbl(verbrauch,
                  anlage,
                  anlage_pkt_link,
                  pkt,
                  pkt_perimeter_energieplanung_stzh_link,
                  perimeter_energieplanung_stzh)

  ver <- function(pkt) {
    dm_verbrauch %>%
    dm_filter(pkt, aobj == pkt) %>%
    dm::dm_apply_filters() %>%
    dm_flatten_to_tbl(verbrauch) %>%
    mutate(aobj = pkt) %>%
    collect()
  }

  res <- map_dfr(pkt, ver) %>%
    distinct(gjahr, anlage_id, .keep_all = TRUE) %>%
    group_by(gjahr, abrechnungsklasse, aobj) %>%
    summarise(erdgas_tr_pkt = sum(erdgas_tr, na.rm = TRUE),
              erdgas_ha_pkt = sum(erdgas_ha, na.rm = TRUE),
              biogas_pkt = sum(biogas, na.rm = TRUE)) %>%
    ungroup()

  res
}

#' @title gibt die summierten Gasverbräuche für die Energieperimeter zurück.
#ä
#' @description Bei sich überschneidenden Gebieten werden die Verbräuche allen
#' Gebieten zugerechnet und dadurch ggf. vervielfacht.
#'
#' @details zurückgegeben werden die werte erdgas_tr (erdgas durchgeleitet), erdgas_ha
#' (erdgas gehandelt) und biogas. Alle Werte sind in kWh.
#'
#' @name verbrauch_gebiet
#' @param gebiet Gebietsbezeichnung; entweder "energieperimeter" oder "energieverbuende" (TBD!)
#' @param jahr Geschäftsjahr
#' @examples
#' verbrauch_gebiet(gebiete = "energieperimeter"))
#' @export
verbrauch_gebiet <- function(gebiete = "energieperimeter", jahr = 2019) {
  gebiete <- if(gebiete == "energieperimeter") {
    "pkt_perimeter_energieplanung_stzh_link"
  } else if (gebiete == "energieverbuende") {
    "pkt_energieverbuende_link"
  }

  dm_verbrauch <- e360_dm() %>%
    dm_select_tbl(verbrauch,
                  anlage,
                  anlage_pkt_link,
                  pkt,
                  pkt_perimeter_energieplanung_stzh_link,
                  perimeter_energieplanung_stzh)

  dm_verbrauch %>%
    dm_zoom_to(verbrauch) %>%
    filter(gjahr == jahr) %>%
    group_by(anlage_id, gjahr) %>%
    summarize(erdgas_tr = as.numeric(sum(erdgas_tr, na.rm = TRUE)),
              erdgas_ha = as.numeric(sum(erdgas_ha, na.rm = TRUE)),
              biogas = as.numeric(sum(biogas, na.rm = TRUE))) %>%
    ungroup() %>%
    dm_update_zoomed() %>%
    dm_add_pk(verbrauch, anlage_id, check = TRUE) %>%
    dm_zoom_to(anlage) %>%
    select(anlage_id) %>%
    left_join(verbrauch) %>%
    dm_update_zoomed() %>%
    dm_zoom_to(anlage_pkt_link) %>%
    left_join(anlage) %>%
    # wir ordnen die anlagen dem ersten aobj zu, um doppelzählungen zu vermeiden (unterzähler!)
    arrange(aobj) %>%
    group_by(anlage_id) %>%
    mutate(aobj = first(aobj),
           erdgas_tr = first(erdgas_tr),
           erdgas_ha = first(erdgas_ha),
           biogas = first(biogas)) %>%
    ungroup() %>%
    #workaround, weil group_by(), summarise(first) auf db nicht geht.
    distinct() %>%
    dm_update_zoomed() %>%
    dm_add_fk(anlage_pkt_link, aobj, pkt) %>%
    dm_add_pk(anlage_pkt_link, anlage_id, check = TRUE) %>%
    dm_zoom_to(anlage_pkt_link) %>%
    # die verbräuche auf ein aobj summieren
    group_by(aobj) %>%
    summarise(erdgas_tr = sum(erdgas_tr, na.rm = TRUE),
              erdgas_ha = sum(erdgas_ha, na.rm = TRUE),
              biogas = sum(biogas, na.rm = TRUE)) %>%
    ungroup() %>%
    dm_update_zoomed() %>%
    dm_add_pk(anlage_pkt_link, aobj, check = TRUE) %>%
    dm_zoom_to(pkt) %>%
    select(-SHAPE) %>%
    left_join(anlage_pkt_link) %>%
    dm_update_zoomed() %>%
    dm_flatten_to_tbl(!!gebiete) %>%
    group_by(gebietsnummer) %>%
    summarise(erdgas_tr = sum(erdgas_tr, na.rm = TRUE),
              erdgas_ha = sum(erdgas_ha, na.rm = TRUE),
              biogas = sum(biogas, na.rm = TRUE)) %>%
    ungroup() %>%
    collect()
}
