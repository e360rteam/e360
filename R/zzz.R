.onLoad <- function(libname, pkgname) {
  # memoise the expensive e360_dm call during package load
  # see https://github.com/r-lib/memoise/issues/76
  e360_dm <<- memoise::memoise(e360_dm)

  is_usethis_installed <<- memoise::memoise(is_usethis_installed)
}
