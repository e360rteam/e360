library(tidyverse)
library(e360)
library(dm)

dm <- e360_dm()

dm %>% dm_draw()
#check <- dm %>% dm_examine_constraints() %>% as_tibble()

dm$anlage_pkt_link %>%
  anti_join(dm$pkt, by = "aobj") %>%
  collect() %>%
  select(aobj)


dm$geraet %>%
  anti_join(dm$objektart, by = "objektart_id") %>%
  collect() %>%
  select(matnr, eqnr = geraet_id, objektart_id) %>%
  write.csv("geraete_ohne_objektart.csv")

dm$anlage %>%
  anti_join(dm$anlage_pkt_link, by = "anlage_id") %>%
  collect()

#Kontrolle ob alle Anlagen mit 2 Zählpunkten je eine auf 130 und eine auf 100 haben:
zwei_zaehlpunkte <-
  dm$anlage %>%
  group_by(zaehlpunkt) %>%
  add_count() %>%
  ungroup %>%
  filter(n > 1) %>%
  select(- n)

zwei_zaehlpunkte %>%
  group_by(zaehlpunkt, abrechnungsklasse) %>%
  count() %>%
  arrange(desc(n))

#müssten gleich viele sein:
zwei_zaehlpunkte %>%
  group_by(abrechnungsklasse) %>%
  count()


# technische Plätze -------------------------------------------------------

# technische Plätze in Gerätetabelle unterschiedlich zu Anschlussobjekt?
e360::e360_db()

e360_tbl("gmw_datawarehouse.e360.u_gass_sap_geraet") %>%
  filter(tplnr == "" | is.na(tplnr)) %>%
  select(tplnr, aobj) %>%
  collect() %>%
  view()

e360_tbl("gmw_datawarehouse.e360.u_gass_sap_geraet") %>%
  collect() %>%
  mutate(aobj = str_split(aobj, pattern = " ") %>% map_chr(c(1,1))) %>%
  mutate(tplnr = str_split(tplnr, pattern = " ") %>% map_chr(c(1,1))) %>%
  filter(aobj != tplnr) %>%
  view()

# Technische Plätze, welche in der aobj-Liste nicht auftauchen - diese sind zu überprüfen!

aobj <- dm$pkt %>%
  collect() %>%
  mutate(aobj = str_split(aobj, pattern = " ") %>% map_chr(c(1,1)))

missing_tpl <- dm %>%
  dm_flatten_to_tbl(anlage_geraet_link) %>%
  collect() %>%
  mutate(tplnr = str_split(tplnr, pattern = " ") %>% map_chr(c(1,1))) %>%
  anti_join(aobj, by = c("tplnr" = "aobj"))

#Wieviele sind das?
missing_tpl %>%
  distinct(tplnr)


# Mehrere Anlagen: abweichende Gerätestandorte ----------------------------

#Wo sind aobj und tpl pro Gerät unterschiedlich? (Das eine aus tpl, das andere via Anlage)
#Antwort: Grundsätzlich immer dort, wo sich mehrer aobj eine Anlage teilen: Der left_join vervielfacht die Geräte,
#der Ungleich-Filter selektiert wiederum nur jene, bei welchem die tplnr abweichend ist.
unterschiedlich <- dm %>%
  dm_flatten_to_tbl(anlage_geraet_link) %>%
  left_join(dm$anlage_pkt_link, by = "anlage_id") %>%
  collect() %>%
  mutate(tplnr = str_split(tplnr, pattern = " ") %>% map_chr(c(1,1))) %>%
  mutate(aobj = str_split(aobj, pattern = " ") %>% map_chr(c(1,1))) %>%
  #mutate(tplnr = substr(tplnr, 0, 17)) %>%
  #mutate(aobj = substr(aobj, 0, 17)) %>%
  filter(aobj != tplnr)

unterschiedlich %>% view()

#nur eine Anlage:
unterschiedlich %>%
  group_by(anlage_id) %>%
  count() %>%
  group_by(n) %>%
  count() %>%
  view()


#Fall Lielistrasse (Skizze Cheminee)
dm %>%
  dm_flatten_to_tbl(anlage_geraet_link) %>%
  left_join(dm$anlage_pkt_link, by = "anlage_id") %>%
  collect() %>%
  filter(anlage_id == 1016463) %>%
  view()

#Fall Köschenrütistrasse
dm %>%
  dm_flatten_to_tbl(pkt) %>%
  filter(aobj == "Z/0001.01022.0143") %>%
  select(str, haus_num1, haus_zusatz, plz, ort)

#Warum geht das nicht?
dm_filtered <- dm %>%
  dm_select_tbl(-perimeter_energieplanung_stzh) %>%
  dm_filter(pkt, aobj == "Z/0001.01022.0143") %>%
  dm_apply_filters()

#Fall Gärtnerei Schwerzenbach
dm %>%
  dm_flatten_to_tbl(pkt) %>%
  filter(aobj == "B/0096.52366.0999") %>%
  select(str, haus_num1, haus_zusatz, plz, ort)
